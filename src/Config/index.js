const config = {
	graphqlURI: 'http://185.185.71.180:80/graphql',
	graphqlWSURI: 'ws://185.185.71.180:80/graphql',
  public: 'http://185.185.71.180/',
	TOKEN:'TOKEN'
}

export default config;