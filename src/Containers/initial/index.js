import React, {
	useEffect,
	useState
} from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';

import {
	useQuery
} from '@apollo/client';

import { authorizationGQL } from '../../Services/gqls';

import config from '../../Config';

import { NotificationContainer, NotificationManager } from 'react-notifications';
import 'react-notifications/lib/notifications.css';

import initAuth from '../../Services/tools/initAuth';

import statusAuth from '../../Services/tools/statusAuth';

import Home from '../App/Home';
import Users from '../App/Users';
import AuthHomePage from '../App/Auth';
import Products from '../App/Products'
import Categories from '../App/Categories';
import Orders from '../App/Orders';
import Settings from '../App/Settings'

const Initial = () => {

	const [isAuth, setIsAuth] = useState(true);

	const { data, loading } = useQuery(authorizationGQL.getUser);

  useEffect(()=>{

    if(!loading){
      initAuth(data);
    }

    (async()=>{
      let getStatusAuth = await statusAuth();
      setIsAuth(getStatusAuth);
    })()

  }, [data, loading])

	return (

		<div className="app-main">
			<Switch>
				<Route exact path='/' render={() => isAuth ? <Redirect to="/app" />  : <AuthHomePage/> }/>
				<Route exact path='/app' render={() => isAuth  ? <Home/> : <Redirect to="/" />}/>
				<Route exact path='/categories' render={() => isAuth ? <Categories/> : <Redirect to="/" />}/>
				<Route exact path='/users' render={() => isAuth ? <Users/> : <Redirect to="/" />}/>
        		<Route exact  path='/products' render={() => isAuth ? <Products /> : <Redirect to="/" />}/>
				<Route exact  path='/orders' render={() => isAuth ? <Orders /> : <Redirect to="/" />}/>
				<Route exact  path='/settings' render={() => isAuth ? <Settings /> : <Redirect to="/" />}/>
				{/*Redirect 404*/}
				<Route path='*' render={()=> <Redirect to="/" />}/>

		  </Switch>

			<NotificationContainer/>

		</div>

	)
}

export default Initial;