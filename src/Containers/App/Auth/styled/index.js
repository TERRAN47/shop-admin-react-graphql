import styled from 'styled-components';

const Wrapper = styled.div`
	min-width: 80%;
	max-width: 80%;
	min-height:calc(100vh);
	margin:0 auto;
	border:1px solid rgba(0,0,0,0.06);
	border-radius:4px;
	background:#fff;
	box-shadow:0 0 24px rgba(0,0,0,0.1);
	overflow-y:auto;
	box-sizing:border-box;
	display: flex;
	justify-content: center;
	align-items: center;
`

const ButtonAuth = styled.input`
	border:none;
	outline:none;
	padding:10px 15px;
	margin:10px 15px;
	background:orange;
	border-radius:6px;
	color:#fff;
	font-weight:bold;
	font-size:18px;
	letter-spacing:1px;
	cursor:pointer;
	height: 50px;
`

const ModalAuth = styled.div`
	position:fixed;
	display:flex;
	justify-content:center;
	align-items:center;
	width:100%;
	height:100vh;
	background:rgba(255,255,255,0.3);
	left:0;
	top: 0;
	opacity: ${props => props.status ? 1 : 0};
	visibility: ${props => props.status ? 'visible' : 'hidden'};


	transition:.3s all;
`

const ModalAuthContent = styled.div`
	width:100%;
	display: flex;
	justify-content: center;
	flex-direction: column;
	background:#fff;
	border-radius:7px;
	border:1px solid 1px solid rgb(51 51 51 / 12%);
	box-shadow:0 0 30px rgba(0,0,0,0.15);
`

const ModalAuthContentHead = styled.div`
	width:100%;
	display:flex;
	justify-content:center;
	flex-direction: column;
	align-items:center;
	position:relative;
	padding: 15px 0;
`

const H2 = styled.h2`
    margin: 0;
    font-size: 26px;
    color: #585858;
    font-weight: normal;
`

const ModalAuthContentBody = styled.div`
	width:100%;
	display:flex;
	justify-content:center;
	flex-direction: column;
	align-items:center;
	position:relative;
	padding: 15px 0;
	flex-direction:column;
`

const Input = styled.input`
	width: 70%;
    height: 45px;
    border: 1px solid rgb(208 208 208 / 47%);
    box-sizing:border-box;
    border-radius: 7px;
    outline: none;
    padding: ${ props => props.submit ? '0px' : '0 20px' };
    cursor: ${ props => props.submit ? 'pointer' : '' };
    font-size:18px;
    box-shadow: 0 0 24px rgb(0 0 0 / 10%);
    margin: 10px 0;
`

export {
	Wrapper,
	ButtonAuth,
	ModalAuth,
	ModalAuthContent,
	ModalAuthContentHead,
	H2,
	ModalAuthContentBody,
	Input
}