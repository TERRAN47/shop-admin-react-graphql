import React, {
	useState,
	useEffect,
	Suspense
} from 'react';

import { useTranslation } from "react-i18next";

import { withRouter } from 'react-router-dom';

import { 
	useMutation
} from '@apollo/client';

import {
	Wrapper,
	ButtonAuth,
	ModalAuth,
	ModalAuthContent,
	ModalAuthContentHead,
	H2,
	ModalAuthContentBody,
	Input
} from './styled';

import saveToken from '../../../Services/tools/authToken';

import { authorizationGQL } from '../../../Services/gqls';
import { NotificationManager } from 'react-notifications';

const AuthHomePage = ({
	history
}) => {

	const [modal, setModal] = useState(false);
	const [email, setEmail] = useState('');
	const [pwd, setPwd] = useState('');

	const { t, i18n } = useTranslation('common');

	const [authorization] = useMutation(authorizationGQL.login, {
		variables: {
			profile: {
				email,
				password:pwd,
        type:'ADMIN'
			}
		},
    onError: (err) => {
      NotificationManager.error('Ошибка', "Ошибка");
      return
    },
		onCompleted: async (user) => {
			await saveToken(user.authentication.token, true);
			return history.go('/app');
		}
	});

	return (
		<Suspense fallback="loading">
		<Wrapper>

			<ModalAuth status={modal}>
				<ModalAuthContent>

					<ModalAuthContentHead>
						<H2>{t("auth.authorization")}</H2>
					</ModalAuthContentHead>	

					<ModalAuthContentBody>
						
						<Input onChange={(val)=>setEmail(val.target.value)} type="text" placeholder="Введите email" />
						<Input onChange={(val)=>setPwd(val.target.value)} type="password" placeholder="Введите пароль" />

						<Input onClick={()=>authorization()} submit type="submit" value={t('auth.Signin')} />

					</ModalAuthContentBody>	

				</ModalAuthContent>
			</ModalAuth>

			<ButtonAuth onClick={()=>setModal(!modal)} type="submit" value={t("auth.authorization")} />
		</Wrapper>
		</Suspense>
	)
}

export default withRouter(AuthHomePage);