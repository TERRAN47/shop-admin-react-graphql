import React, {
    useState,
    useEffect
  } from 'react';
  
  import Switch from "react-switch";
  import { NotificationManager } from 'react-notifications';
  import {
    styled
  } from '../../../../Services';
  import {
    Form,
    H1,
    Input,
    InputFile,
    P,
    SectionSubCategorySwitch,
    Select,
    Option,
    CreateImage,
    ListCategoriesBlock,
    FormHead
  } from '../styled';

  import { 
    productsGQL,
    categoriesGQL,
    filesGQL
  } from '../../../../Services/gqls';
  import config from '../../../../Config'
  import { 
    useMutation,
    useLazyQuery
  } from '@apollo/client';
  import Icon from '@mdi/react';
  import { mdiClose } from '@mdi/js';
  const {
    text
  } = styled;
  const {TextArea} = text;

  const CreateCategoryForm = ({
    hide,
    createModal,
    t,
    newProduct,
    categories,
    closeModal
  }) => {
    const [getSubCategories] = useLazyQuery(categoriesGQL.getSubCategories, {
      onCompleted: (data)=>{
        console.log(111, data);
        if(data && data.getSubCategories.length){
    
          setSubCategories(data.getSubCategories);
        }
      },
      onError: (err)=> {
        console.log(222, err)
      }
    });
    const [subCategories, setSubCategories] = useState(null);
    const [title, setTitle] = useState('');
    const [description, setDescription] = useState('');
    const [price, setPrice] = useState(0);
    const [picture, setPicture] = useState('');
    const [uplodImage, setUplodImage] = useState('');
    const [selectCategory, setSelectCategory] = useState(null);
    const [selectSubCategory, setSubSelectCategory] = useState(null);
    
    const [createProduct] = useMutation(productsGQL.createProduct, {
      variables: {
        product: {
          title,
          description,
          picture,
          category: selectSubCategory,
          price
        }
      },
      onCompleted: (data)=>{
        if(data.createProduct){
          createModal()
          setTitle('');
          setDescription('');
          setPrice('');
          setPicture('');
          setUplodImage('');
          setSelectCategory(null);
          newProduct(data.createProduct);
        }
      },
      onError: (err)=>{
        console.log(222, err)
      }
    });
    

    const eventLoadSubCategory = (id) => {
      console.log(2222222, id)
      if(id != 'none'){
        setSelectCategory(id)
        getSubCategories({
          variables:{
            id
          }
        });
        return
      }

  
      setSubCategories(null);
    }

    const [uploadFile] = useMutation(filesGQL.uploadFile, {
      onCompleted: (data)=>{
        console.log(111, data);
        setPicture(data.uploadFile._id);
        setUplodImage(data.uploadFile.url ? data.uploadFile.url : '');
      },
      onError: (err)=> {
        console.log(222, err)
      }
    });
  
 
    
    return (
      <Form hide={hide}>
        <FormHead>
          <H1>{t('products.addProduct')}</H1>
          <Icon size={1} onClick={()=>closeModal()} path={mdiClose} />
        </FormHead>
        <InputFile type="file" onChange={(val)=>uploadFile({
          variables: {
            file: val.target.files[0]
          }
        })} />
        
        <CreateImage src={uplodImage ? config.public+uplodImage : '/assets/noImage.png'} />

        <Input type="text" value={title} placeholder={ t('products.productName') } onChange={(val)=>setTitle(val.target.value)} />

        <TextArea value={description} rows="5" placeholder={ t('app.description') } onChange={(val)=>setDescription(val.target.value)} />

        <Input type="number" placeholder={ t('app.price') } onChange={(val) => setPrice(+(val.target.value))} />

        <ListCategoriesBlock>
          <text.P>{t('app.category')}</text.P> 
          {
            categories && categories.length ?
              <Select onChange={(val)=>{eventLoadSubCategory(val.target.value)}}>

                <Option value="none" >{ t('app.selectCategory') }</Option>

                {
                  categories.map((cat, index)=>{
                    return <Option key={index} value={cat._id} >{ cat.title }</Option>
                  })
                }
              </Select>
            :
              <P>{ t('app.noCategories') }</P>
          }
          <text.P>{t('app.subCategory')}</text.P> 
          {
            subCategories && subCategories.length ?
              <Select onChange={(val)=>{
                setSubSelectCategory(val.target.value)
                return
              }}>

                <Option value="none" >{ t('app.selectCategory') }</Option>

                {
                  subCategories.map((cat, index)=>{
                    return <Option key={index} value={cat._id} >{ cat.title }</Option>
                  })
                }
              </Select>
            :
              <P>{ t('app.noCategories') }</P>
          }
        </ListCategoriesBlock>
  
        <Input submit type="submit" onClick={createProduct} value={t('app.add')} />
  
      </Form>
    )
  }
  
  export default CreateCategoryForm;