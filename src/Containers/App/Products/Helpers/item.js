import React from 'react';

import {
  ItemCategory,
  P,
  ItemCategoryControll
} from '../Styled';

import Icon from '@mdi/react';
import { mdiDelete } from '@mdi/js';

const Item = ({
  item,
  t,
  categoriesGQL,
  useMutation,
  removeCat
}) => {

  return (
    <ItemCategory>
      <P size={16}>{ item.title }</P>
      <ItemCategoryControll>
        <Icon onClick={()=>eventRemoveCategory()} size={0.7} color="#d2421d" path={mdiDelete} />
      </ItemCategoryControll>
    </ItemCategory>
  )
}

export default Item;