import React, {
	useState,
	useEffect,
	Suspense
} from 'react';

import {
	styled
} from '../../../Services';
import {
  ProductInfo, 
  Decription, 
  Image,
  ListProduct,
  ItemProductView,
  IconControll
} from './styled';
import { useTranslation } from "react-i18next";
import { BarMenu } from '../../../Components';
import ListView from "rmc-list-view"
import { mdiDatabaseSearchOutline, mdiClipboardEditOutline, mdiPailRemove  } from '@mdi/js';
import Icon from '@mdi/react'
import { constants } from '../../../Services';
import CreateProdutForm from './Helpers/create-product-form';
import { categoriesGQL, productsGQL } from '../../../Services/gqls';
import config from '../../../Config';
import _ from 'underscore';
import ProductBlock from '../../../Components/productDetal'
import { 
  useMutation,
  useQuery,
  useLazyQuery
} from '@apollo/client';

let {
	containers,
	text
} = styled;
const {FindInput, IconInput, IconItemRight, DefButton, FindInputBlock} = containers;

const productsArr = [
  {
    "_id": "613c89d69f6f5f3a10986dbd",
    "title": "Бананы",
    "description": "Бананы",
    "price": 10000,
    "picture": {
      "url": "uploads/9T6jHAGDlZ_000000000000060033_0.jpg",
      "__typename": "Picture"
    },
    "category": {
      "title": "Фрукты",
      "__typename": "Category"
    },
    "__typename": "Product"
  },
  {
    "_id": "613c89c59f6f5f3a10986dab",
    "title": "Помидоры",
    "description": "Помидоры",
    "price": 5000,
    "picture": {
      "url": "uploads/zCpq5sVQZ1_tomatoes-health-big.jpg",
      "__typename": "Picture"
    },
    "category": {
      "title": "Салат",
      "__typename": "Category"
    },
    "__typename": "Product"
  },
  {
    "_id": "613c89a89f6f5f3a10986da0",
    "title": "Редис",
    "description": "выаыв",
    "price": 2500,
    "picture": {
      "url": "uploads/w3BqQIozAM_pochemu-rediska-gorkaya-kak-ubrat-gorech.jpg",
      "__typename": "Picture"
    },
    "category": {
      "title": "Корзина",
      "__typename": "Category"
    },
    "__typename": "Product"
  }
]


const Products = () => {
  const dataSourceProducts = new ListView.DataSource({
    rowHasChanged: (row1, row2) => row1 !== row2,
  });

  const [categories, setCategories] = useState(null);
  const [modalCreate, setModalCreate] = useState(false);
  const [products, setProducts] = useState(dataSourceProducts);
  let [productsData, setProductsData] = useState([]);
  const [idProduct, setIdProduct] = useState(null);
  const [productDetal, setProductDetal] = useState(false);
  const { t } = useTranslation('common');
  const getCategories = useQuery(categoriesGQL.getCategories);
  const [itemProduct, setItemProduct] = useState(null);
  const [editStatus, setEditStatus] = useState(false);
  const [find, setFind] = useState('');
  const [firstQuery, setFirstQuery] = useState(true);
  const [timeoutItem, setTimeoutItem] = useState(setTimeout(()=>{}, 1000));

  const getProducts = useLazyQuery(productsGQL.getProducts, {
    variables:{
      query:{}
    }
  });
  

  // console.log(78888888, searchProduts[0])
  const [removeProduct] = useMutation(productsGQL.removeProduct, {   
    onCompleted: (data)=>{
      if(data.removeProduct && data.removeProduct.status){
        let newProductsData = _.without(productsData, _.findWhere(productsData, {
          _id: idProduct
        }))
        setProductsData(newProductsData)
        setProducts(products.cloneWithRows(newProductsData))
      }
     
    },
    onError: (err)=>{
      console.log(222, err)
    }
  });

  useEffect(()=>{
    if(firstQuery){
      setFirstQuery(false)
      getProducts[0]()
    }
    console.log(99999999999, getProducts[1])

    if(getProducts[1].data){
      setProducts(products.cloneWithRows(getProducts[1].data.getProducts ? getProducts[1].data.getProducts : []))
      setProductsData(getProducts[1].data.getProducts ? getProducts[1].data.getProducts : [])
    } 
    
  }, [
    getProducts[1].data
  ])

  useEffect(()=>{
    if(!getCategories.loading){
      setCategories(getCategories.data.getCategories);
      
    }
  }, [
    getCategories.data,
    getCategories.loading
  ])
  const deletProduct = (id)=>{
    let delStatus =  window.confirm(t('products.deleteProduct'));
    if(delStatus){
      setIdProduct(id)
      removeProduct({
        variables: {
          id
        }
      })
    }
  }

  const detalProductChange = ()=>{
    setProductDetal(!productDetal)
    setEditStatus(false)
  }

  const openDetalProduct = (elem)=>{
    setItemProduct(elem)
    setProductDetal(!productDetal)
    setEditStatus(false)
  }
  
  const editProduct = (elem)=>{
    setItemProduct(elem)
    setEditStatus(true)
    setProductDetal(!productDetal)
  }
  const renderProduct = (elem, index)=>{
    return(
      <ItemProductView>
        <Image src={elem.picture && elem.picture.url ? config.public+elem.picture.url : ''} onClick={()=> openDetalProduct(elem)} />
        <ProductInfo>
          <text.P color={constants.colors.main}>{elem.title}</text.P>
          <text.P>{t('app.price')}: {elem.price} {constants.currency}</text.P>
          <Decription>{elem.description}</Decription>
        </ProductInfo>
        <IconControll onClick={()=> deletProduct(elem._id)}>
          <Icon size={0.8} color={constants.colors.red} path={mdiPailRemove} />
        </IconControll>
        <IconControll paddingTop={45} onClick={()=>editProduct(elem)} top="60px">
          <Icon size={0.8}  path={mdiClipboardEditOutline } />
        </IconControll>
      </ItemProductView>
    )
  }

  const onEndReached = ()=>{
    setProducts(products.cloneWithRows([...productsData, ...productsArr]))
    setProductsData([...productsData, ...productsArr])
    return
  }

  const newProduct = (data)=>{
    if(Array.isArray(productsData)){

      let newproductsData = [data, ...productsData]

      setProductsData(newproductsData)
      setProducts(products.cloneWithRows(newproductsData))
    }
  }

  const createModal = ()=>{
    setModalCreate(!modalCreate)
  }

  const findProducts = (val)=>{
    setFind(val.target.value);
    clearTimeout(timeoutItem);
    setTimeoutItem(setTimeout(()=>{
      getProducts[0]({
        variables:{
          query:{query: find}
        }
      })
    }, 1000));
  }

  
	return (
    <BarMenu>
      <CreateProdutForm closeModal={createModal} t={t} newProduct={newProduct} categories={categories} createModal={createModal} hide={modalCreate} />
      <containers.contentBlock>
        <containers.contentHeader>
          <FindInputBlock>
            <FindInput type="text" onChange={findProducts} placeholder={t('app.search')} />
            <IconInput>
              <Icon size={1} path={mdiDatabaseSearchOutline} />
            </IconInput>
          </FindInputBlock>
          
          <DefButton onClick={createModal}> {t('app.add')} </DefButton>
        </containers.contentHeader>

        <containers.contentInfo>
          {
            productDetal ? 
              <ProductBlock editStatus={editStatus}  detalProductChange={detalProductChange} itemProduct={itemProduct} />
            :
              <div>
                <text.h2 color={constants.colors.main}>{t('app.products')}</text.h2>
                <containers.tableDefault>
                  <containers.div>
                    <ListProduct>
                      {
                        products ?
                          <ListView
                            // ref={el => this.lv = el}
                            dataSource={products}
                            useBodyScroll={true}
                            renderRow={renderProduct}
                            onEndReachedThreshold={3}
                            pageSize={3}
                            onEndReached={onEndReached}
                            renderFooter={() => <div>{getCategories.loading ? 'loading...' : ''}</div>}
                          />
                        :
                          null
                      }
                    </ListProduct>
                  </containers.div>     
                </containers.tableDefault>
              </div>
          }
        </containers.contentInfo>
      </containers.contentBlock>
		</BarMenu>
	)
}

export default Products;