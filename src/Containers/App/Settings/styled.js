import styled from 'styled-components'


const SettingInput = styled.input`
    padding: 10px;
    border: none;
    font-size: 14px;
    width: 300px;
    margin-top: 15px;
    margin-right: 15px;
    border-radius: 5px;
    box-sizing: border-box;
`
const SettingsBlock = styled.div`
    display: flex;
    flex-direction: column;
`

export {
    SettingsBlock,
    SettingInput,
}