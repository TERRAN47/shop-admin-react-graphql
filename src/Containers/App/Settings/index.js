import React, {
	useState,
	useEffect,
} from 'react';
import {
	styled
} from '../../../Services';
import {
  SettingsBlock,
  SettingInput
} from './styled';
import { useQuery } from '@apollo/client';
import { BarMenu } from '../../../Components'
import { mdiAccountSearchOutline, mdiAccountRemove } from '@mdi/js';
import Icon from '@mdi/react'
import { constants } from '../../../Services';
import { settingsGQL } from '../../../Services/gqls';
import { useTranslation } from "react-i18next";
import _ from 'underscore'
import { 
  useMutation
} from '@apollo/client';
let {
	containers,
	text,
} = styled;

const {FindInput, DefButton, Option, DefSelect, IconItemRight, IconInput, FindInputBlock} = containers


const Settings = () => {
  const { t } = useTranslation('common');

  const getSettings = useQuery(settingsGQL.getSettings);

  const [settings, setSettings] = useState(null);
  const [phoneOne, setPhoneOne] = useState(null);
  const [phoneTwo, setPhoneTwo] = useState(null);
  const [email, setEmail] = useState(null);
  const [priceDelivery, setPriceDelivery] = useState(null);
  const [carrency, setCarrency] = useState(null);
  
  const [timeoutItem, setTimeoutItem] = useState(()=>{});
  
  const [saveSettings] = useMutation(settingsGQL.saveSettings, {
    onCompleted: (data)=>{
      console.log(3333, data)
    },
    onError: (err)=>{
      console.log(222, err)
    }
  });
  

  const sendSetting = (type, value)=> {
    saveSettings({
      variables: {
        setting: {
          type,
          value
        }
      }
    })
  }

  const changePhoneOne = (val)=>{
    setPhoneOne(val.target.value);
    clearTimeout(timeoutItem)
    setTimeoutItem(setTimeout(()=>{
      sendSetting('PHONE_ONE', val.target.value)
    }, 1000))
  }

  const changePhoneTwo = (val)=>{
    setPhoneTwo(val.target.value);
    clearTimeout(timeoutItem)
    setTimeoutItem(setTimeout(()=>{
      sendSetting('PHONE_TWO', val.target.value)
    }, 1000))
  }
  const changeEmail = (val)=>{
    setEmail(val.target.value);
    clearTimeout(timeoutItem)
    setTimeoutItem(setTimeout(()=>{
      sendSetting('EMAIL', val.target.value)
    }, 1000))
  }
  const changePriceDelivery = (val)=>{
    setPriceDelivery(val.target.value);
    clearTimeout(timeoutItem)
    setTimeoutItem(setTimeout(()=>{
      sendSetting('PRICE_DELIVERY', val.target.value)
    }, 1000))
  }
  const changeCarrency = (val)=>{
    setCarrency(val.target.value);
    clearTimeout(timeoutItem)
    setTimeoutItem(setTimeout(()=>{
      sendSetting('CARRENCY', val.target.value)
    }, 1000))
  }
  
  useEffect(()=>{
 
    if(getSettings.data && getSettings.data.getSettings){
      setSettings(getSettings.data.getSettings);
      let settings = getSettings.data.getSettings
      let findEmail = _.findWhere(settings, {type: "EMAIL"})
      let findPhoneOne = _.findWhere(settings, {type: "PHONE_ONE"})
      let findPhoneTwo = _.findWhere(settings, {type: "PHONE_TWO"})
      let findPriceDelivery = _.findWhere(settings, {type: "PRICE_DELIVERY"})
      let findCarrency = _.findWhere(settings, {type: "CARRENCY"})
      
      if(findEmail) setEmail(findEmail.value)
      if(findPhoneOne) setPhoneOne(findPhoneOne.value)
      if(findPhoneTwo) setPhoneTwo(findPhoneTwo.value)
      if(findPriceDelivery) setPriceDelivery(findPriceDelivery.value)
      if(findCarrency) setCarrency(findCarrency.value)
      
    } 
  }, [getSettings.data])
  console.log(2222222222, carrency, settings)
	return (
    <BarMenu>

      <containers.contentBlock>
        <containers.contentHeader>
          <FindInputBlock>
            <FindInput type="text" placeholder={t("app.search")} />
            <IconInput>
              <Icon size={1} path={mdiAccountSearchOutline} />
            </IconInput>
          </FindInputBlock>
          
        </containers.contentHeader>

        <containers.contentInfo>
          <text.h2 color={constants.colors.main}>{t("app.settings")}</text.h2>
          <SettingsBlock>
            <SettingInput defaultValue={email} type="text" onChange={changeEmail} placeholder={t("app.email")} />

            <containers.row>
              <SettingInput type="number" defaultValue={phoneOne} onChange={changePhoneOne} placeholder={`${t("app.phone")} 1`} />
              <SettingInput type="number" defaultValue={phoneTwo} onChange={changePhoneTwo} placeholder={`${t("app.phone")} 2`} />
            </containers.row>

            <DefSelect onChange={changeCarrency} value={carrency}>
              <Option value="₸">Тенге</Option>
              <Option value="$">Доллары</Option>
            </DefSelect>

            <SettingInput type="number" defaultValue={priceDelivery} onChange={changePriceDelivery} placeholder={t("settings.priceDelivery")} />
          </SettingsBlock>        
        </containers.contentInfo>

      </containers.contentBlock>

		</BarMenu>
	)
}

export default Settings;