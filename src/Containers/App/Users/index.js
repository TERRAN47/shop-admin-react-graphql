import React, {
	useState,
	useEffect
	
} from 'react';
import {
	styled
} from '../../../Services';

import { useQuery } from '@apollo/client';
import { BarMenu } from '../../../Components'
import { mdiAccountSearchOutline, mdiAccountRemove } from '@mdi/js';
import Icon from '@mdi/react'
import { constants } from '../../../Services';
import { userGQL } from '../../../Services/gqls';
import ListView from "rmc-list-view"
import { useTranslation } from "react-i18next";
let {
	containers,
	text
} = styled;

const {FindInput, IconItemRight, IconInput, FindInputBlock} = containers

const dataSourceUsers = new ListView.DataSource({
  rowHasChanged: (row1, row2) => row1 !== row2,
});

const Users = () => {

  const { t } = useTranslation('common');
  const getUsers = useQuery(userGQL.getUsers, {
    variables:{
      query:{}
    }
  });

  const [users, setUsers] = useState(dataSourceUsers);

  useEffect(()=>{
    console.log(33333333, getUsers)
    if(getUsers.data && getUsers.data.getUsers){
    
      setUsers(users.cloneWithRows(getUsers.data.getUsers))
    } 
  }, [getUsers.data])


  const renderUser = (item, rr, index)=>{
    return(
      <containers.tableBodyTr key={index} overflow='auto'>
        <text.tableBodyTd width='5%'>{+(index)+1}</text.tableBodyTd>
        <text.tableBodyTd width='15%'>{item.firstName}</text.tableBodyTd>
        <text.tableBodyTd width='15%'>{item.email}</text.tableBodyTd>
        <text.tableBodyTd width='15%'>{item.phone}</text.tableBodyTd>
        <text.tableBodyTd width='15%'>{item.createdAt}</text.tableBodyTd>
        <IconItemRight onClick={()=>{}} top="4px" color={constants.colors.red}>
          <Icon size={1} path={mdiAccountRemove} />
        </IconItemRight>
      </containers.tableBodyTr>
    )
  }

  const onEndReached = ()=>{

  }
  console.log(2222222222, users)
	return (
    <BarMenu>

      <containers.contentBlock>
        <containers.contentHeader>
          <FindInputBlock>
            <FindInput type="text" placeholder={t("app.search")} />
            <IconInput>
              <Icon size={1} path={mdiAccountSearchOutline} />
            </IconInput>
          </FindInputBlock>
        </containers.contentHeader>

        <containers.contentInfo>
          <text.h2 color={constants.colors.main}>{t("app.users")}</text.h2>
          <containers.tableDefault>
            <containers.tableHead>
              <text.tableHeadTd width='5%'>№</text.tableHeadTd>
              <text.tableHeadTd width='15%'>{t("app.name")}</text.tableHeadTd>
              <text.tableHeadTd width='15%'>{t("app.email")}</text.tableHeadTd>
              <text.tableHeadTd width='15%'>{t("app.phone")}</text.tableHeadTd>
              <text.tableHeadTd width='15%'>{t("app.registered")}</text.tableHeadTd>
            </containers.tableHead>
            <containers.tableBody>
            {
              users ?
                <ListView
                  // ref={el => this.lv = el}
                  dataSource={users}
                  useBodyScroll={true}
                  renderRow={renderUser}
                  //onEndReachedThreshold={20}
                  pageSize={20}
                  onEndReached={onEndReached}
                 // renderFooter={() => <div style={{ padding: 30 }}>{getCategories.loading ? 'loading...' : ''}</div>}
                />
              :
                null
            }
            </containers.tableBody>
          </containers.tableDefault>

        </containers.contentInfo>

      </containers.contentBlock>

		</BarMenu>
	)
}

export default Users;