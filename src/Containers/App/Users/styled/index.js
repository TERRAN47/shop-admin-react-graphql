import styled from 'styled-components'


const FindInput = styled.input`
    padding: 10px;
    border: none;
    font-size: 14px;
    width: 85%;
    border-radius: 5px;
`


export {
    FindInput,
}