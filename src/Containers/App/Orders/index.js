import React, {
	useState,
	useEffect
	
} from 'react';
import {
	styled
} from '../../../Services';
import { useTranslation } from "react-i18next";
import { useQuery, useLazyQuery } from '@apollo/client';
import { BarMenu } from '../../../Components'
import { mdiAccountSearchOutline, mdiAccountRemove } from '@mdi/js';
import Icon from '@mdi/react'
import { constants } from '../../../Services';
import { ordersGQL } from '../../../Services/gqls';
import ListView from "rmc-list-view"
import DetalOrder from "./Helpers/detalOrder"
import moment from 'moment'

// moment.locale('ru')
let {
	containers,
	text
} = styled;

const {FindInput, IconItemRight, IconInput, FindInputBlock} = containers

const dataSourceOrders = new ListView.DataSource({
  rowHasChanged: (row1, row2) => row1 !== row2,
});

const Orders = () => {
  const getOrders = useLazyQuery(ordersGQL.getOrders, {
    fetchPolicy: 'network-only',
    variables:{
      query:{}
    }
  });
  const [firstQuery, setFirstQuery] = useState(true);
  const [orders, setOrders] = useState(dataSourceOrders);
  const [itemOrder, setItemOrder] = useState(null);
  const [orderDetal, setOrderDetal] = useState(false);
  const [find, setFind] = useState('');
  const [timeoutItem, setTimeoutItem] = useState(setTimeout(()=>{}, 1000));
  const { t } = useTranslation('common');
  useEffect(()=>{
    if(firstQuery){
      setFirstQuery(false)
      getOrders[0]()
    }
    if(getOrders[1].data && getOrders[1].data.getOrders) setOrders(orders.cloneWithRows(getOrders[1].data.getOrders))
  }, [
    getOrders[1].data,
  ])
  const openOrder = (item)=>{
    setOrderDetal(!orderDetal)
    setItemOrder(item)
  }
  const orderDetalStatus = ()=>{
    setOrderDetal(!orderDetal)
  }
  const renderUser = (item, rr, index)=>{
  
    return(
      <containers.tableBodyTr key={index} overflow='auto' onClick={()=>openOrder(item)}>
        <text.tableBodyTd width='15%'>{item._id}</text.tableBodyTd>
        <text.tableBodyTd width='15%'>{item.user.firstName}</text.tableBodyTd>
        <text.tableBodyTd width='15%'>{item.price}</text.tableBodyTd>
        <text.tableBodyTd width='15%'>{moment(+(item.createdAt)).format('DD MM YYYY, HH:MM')}</text.tableBodyTd>
        <IconItemRight onClick={()=>{}} top="4px" color={constants.colors.red}>
          <Icon size={1} path={mdiAccountRemove} />
        </IconItemRight>
      </containers.tableBodyTr>
    )
  }
  const findOrder = (val)=>{
    setFind(val.target.value);
    clearTimeout(timeoutItem);
    setTimeoutItem(setTimeout(()=>{
      getOrders[0]({
        variables:{
          query:{query: find}
        }
      })
    }, 1000));
  }
  const onEndReached = ()=>{

  }
  console.log(12121, orders)
	return (
    <BarMenu>

      <containers.contentBlock>
        <containers.contentHeader>
          <FindInputBlock>
            <FindInput type="text" onChange={findOrder} placeholder={t("app.search")} />
            <IconInput>
              <Icon size={1} path={mdiAccountSearchOutline} />
            </IconInput>
          </FindInputBlock>
          
        </containers.contentHeader>

        <containers.contentInfo>
          {
            orderDetal ?
              <DetalOrder orderDetalStatus={orderDetalStatus} itemOrder={itemOrder} />
            :
              <div>
                <text.h2 color={constants.colors.main}>{t("app.orderBoard")}</text.h2>

                <containers.tableDefault>
                  <containers.tableHead>
                    <text.tableHeadTd width='15%'>Заказ ID</text.tableHeadTd>
                    <text.tableHeadTd width='15%'>Имя</text.tableHeadTd>
                    <text.tableHeadTd width='15%'>Сумма заказа</text.tableHeadTd>
                    <text.tableHeadTd width='15%'>{t("app.dateOrder")}</text.tableHeadTd>
                  </containers.tableHead>
                  <containers.tableBody>
                  {
                    orders && orders.rowIdentities && orders.rowIdentities.length ?
                      <ListView
                        // ref={el => this.lv = el}
                        dataSource={orders}
                        useBodyScroll={true}
                        renderRow={renderUser}
                        onEndReachedThreshold={20}
                        pageSize={20}
                        onEndReached={onEndReached}
                        //renderFooter={() => <div style={{ padding: 30 }}>{getCategories.loading ? 'loading...' : ''}</div>}
                      />
                    :
                      null
                  }
                  </containers.tableBody>
                </containers.tableDefault>
              </div>
          }
        </containers.contentInfo>

      </containers.contentBlock>

		</BarMenu>
	)
}

export default Orders;