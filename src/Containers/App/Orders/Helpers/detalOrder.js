import React, {
  useState
} from 'react';
import { useTranslation } from "react-i18next";
import {
	styled,
  constants
} from '../../../../Services';
import {
  ClientBlock,
  OrderBlock,
  Avatar,
  LeftBlock,
  OrderInfo,
  ProdutsBlock,
  HeaderOrder,
  ProductItem,
  OrderContent,
  IconControll,
  ProductInfo, 
  Decription, 
  Image,
} from '../styled'
import moment from "moment"
import Icon from '@mdi/react'
import config from '../../../../Config';
import { mdiCloseCircle,  mdiPailRemove  } from '@mdi/js';

let {
	containers,
	text
} = styled;
const DetalOrder = ({
  itemOrder,
  orderDetalStatus,
}) => {
  const { t } = useTranslation('common');
  const [title, setTitle] = useState('');
  console.log(1111, itemOrder)
  return (
    <OrderBlock>
      <HeaderOrder>
        <text.h2 color={constants.colors.main}>{t("app.order")}</text.h2>
        <Icon size={1.5} color={constants.colors.red} path={mdiCloseCircle} onClick={orderDetalStatus} />
      </HeaderOrder>
      <OrderContent>
        <LeftBlock>
          <ClientBlock>
            <Avatar src='/assets/noavatar.png' />
            <text.P>Имя: {itemOrder.user.firstName}</text.P> 
            <text.P>ID: {itemOrder.user._id}</text.P> 
          </ClientBlock>
          <OrderInfo>
            <text.P>Сумма: {itemOrder.price}</text.P>
            <text.P>Дата: {moment(+(itemOrder.createdAt)).format('DD MM YYYY, HH:MM')}</text.P> 
            <text.P>Номер: {itemOrder.order_number}</text.P>
          </OrderInfo>
        </LeftBlock>
        <ProdutsBlock>
          {
            itemOrder.products.map((elem, index)=>{
              return(
                <ProductItem key={index}>
                  <Image src={elem.picture && elem.picture.url ? config.public+elem.picture.url : ''} />
                  <ProductInfo>
                    <text.P color={constants.colors.main}>{elem.title}</text.P>
                    <text.P>{t('app.price')}: {elem.price} {constants.currency}</text.P>
                    <Decription>{elem.description}</Decription>
                  </ProductInfo>
                </ProductItem>
              )
            })
          }
        </ProdutsBlock>
      </OrderContent>
    </OrderBlock>
  )
}


export default DetalOrder;