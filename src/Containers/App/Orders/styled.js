import styled from 'styled-components'


const FindInput = styled.input`
    padding: 10px;
    border: none;
    font-size: 14px;
    width: 85%;
    border-radius: 5px;
`
const ClientBlock = styled.div`
    background: #fff;
    padding: 15px;
    border-radius: 15px;
`
const OrderBlock = styled.div`
    min-height: 80vh;
`
const OrderContent = styled.div`
    display: flex;
    align-items: flex-start;
`
const Avatar = styled.img`
    width: 50%;
    border-radius: 50%;
    display: table;
    margin: 15px auto;
`
const ProdutsBlock = styled.div`
    width: 75%;
    margin-left: 15px;
    margin-top: 15px;
    background: #fff;
    padding: 15px;
    border-radius: 15px;
	@media (min-width: 1500px) {
        display: grid;
        grid-gap: 15px;
        grid-template-columns: 1fr 1fr;
		${props => props.statusMenu}
	}
`
const HeaderOrder = styled.div`
    display: flex;
    justify-content: space-between;
`


const OrderInfo = styled.div`
    background: #fff;
    padding: 15px;
    border-radius: 15px;
    margin-top: 15px;
`
const LeftBlock = styled.div`
    width: 25%;
    margin-top: 15px;
`
const ProductItem = styled.div`
    display:flex;
    flex-direction:row;

    box-sizing:border-box;
    max-height: 125px;
    padding:10px;
    box-shadow:0 0 10px #333;
    position:relative;
    cursor:pointer;
    box-shadow: 0 0 5px rgb(51 51 51 / 18%);
    border-radius: 10px;
    background:#fff;
    transition:.3s all;
    &:hover {
    transition:.3s all;
    box-shadow: 0 0 15px rgb(51 51 51 / 28%);
    }
	@media (min-width: 1500px) {
		${props => props.statusMenu}
	}
`
const Decription = styled.p`
  margin-top: 10px;
  font-size: 14px;
  overflow: hidden;
  height: 40px;
  padding-right: 15px;
`
const ProductInfo = styled.div`
  padding: 0 15px;
  position: relative;
  width: auto;
  box-sizing:border-box;
  display: flex;
  flex-direction: column;
  align-items: flex-start;

  p {
    font-size:17px;
    color:#c6c6c6;
    margin:0px;
  }

  > p:nth-child(1) {
    font-size:20px;
    color:#373737;
    margin:0 0 5px 0;
  }
`
const Image = styled.div`
  border-radius: 9px;
  width: 100px;
  height:100px;
  background: #fff url(${props=> props.src}) center/cover no-repeat;
`
const IconControll = styled.div`
  position:absolute;
  top:${({paddingTop})=>paddingTop ? paddingTop + 'px' : '10px'};
  right:10px;
`
export {
    FindInput,
    IconControll,
    HeaderOrder,
    Decription,
    Image,
    OrderInfo,
    LeftBlock,
    ProductItem,
    ProdutsBlock,
    ProductInfo,
    Avatar,
    ClientBlock,
    OrderContent,
    OrderBlock
}