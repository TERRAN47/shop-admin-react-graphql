import React from 'react';
import {
	styled
} from '../../../Services';

import { useQuery } from '@apollo/client';
import {BarMenu} from '../../../Components'
import { authorizationGQL } from '../../../Services/gqls';

let {
	containers,
	text
} = styled;

const Home = () => {

	const { data, loading } = useQuery(authorizationGQL.getUser);

	return (

		<BarMenu>
			<text.h2 color="green">Home</text.h2>
			<p>{ loading ? `Loading` : `Hello ${ data && data.getUser ? data.getUser.email : 'client' }` }</p>
		</BarMenu>
	)
}

export default Home;