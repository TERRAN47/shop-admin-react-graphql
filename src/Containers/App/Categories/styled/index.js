import styled from 'styled-components';
import { constants } from '../../../../Services';

const Wrapper = styled.div`
	width: 100%;
  position:relative:
`

const H1 = styled.h1`
  font-size:${props => props.size ? props.size + 'px' : '22px'};
  margin:0;
  margin-top:${props => props.marginTop ? props.marginTop + 'px' : 0};
  margin-left:${props => props.marginLeft ? props.marginLeft + 'px' : 0};
  margin-bottom:${props => props.marginBottom ? props.marginBottom + 'px' : 0};
  color: ${constants.colors.main};
`;

const WrapperHead = styled.div`
  width:100%;
  display:flex;
  flex-direction:row;
  justiy-content:space-between;
  align-items:center:
  padding:10px 0;
`;

const WrapperHeadLeft = styled.div`
  width:100%;
`;

const P = styled.p`
  font-size: ${props => props.size ? props.size + 'px' : '14px'};
  color: ${constants.colors.second};
  margin: 0;
`;

const WrapperHeadRight = styled.div`
  
`;

const Button = styled.input`
  padding: 10px 20px;
  border-radius:7px;
  border:none;
  outline:none;
  cursor:pointer;
  background: ${constants.colors.button};
  color: ${constants.colors.white};
  font-weight:bold;
  font-size: ${constants.sizes.default};
`;

const WrapperBody = styled.div`
  width:100%;
  min-height: calc(100vh - 150px);
  display:flex;
  flex-direction:column;
  justify-content:${props => props.noCategories ? 'center' : 'flex-start'};
  align-items:${props => props.noCategories ? 'center' : 'flex-start'};
  padding:25px 0;
`;

const NoCategories = styled.div`
  
`;

const Form = styled.div`
  position:fixed;
  width:450px;
  height:100vh;
  right: ${ props => !props.hide ? '-500px' : 0 };
  top:0;
  z-index:1000;
  background:#fff;
  box-shadow:0 0 45px rgba(0,0,0,0.2);
  box-sizing:border-box;
  padding:20px;

  transition:.3s all;
`;

const SectionSubCategorySwitch = styled.div`
  display:flex;
  flex-direction:row;
  justify-content:space-between;
  align-items:center;
  width:100%;
  padding:10px;
  background:#eee;
  box-sizing:border-box;
  border-radius:8px;
`;

const Input = styled.input`
  width:100%;
  height:45px;
  border:none;
  outline:none;
  padding:0 15px;
  border-radius:8px;
  margin:15px 0;
  box-sizing:border-box;

  cursor: ${props => props.submit ? "pointer" : ""};
  background: ${props => props.submit ? constants.colors.button : "#eaeaea"};
  color: ${props => props.submit ? constants.colors.white : constants.colors.second};

`;

const UploadInput = styled.input`
  width:100%;
  height:100%;
  position:absolute;
  border:none;
  outline:none;
  padding:0 15px;
  border-radius:8px;
  box-sizing:border-box;
  cursor:pointer;
  background:transparent;
  opacity:0;
`;

const Select = styled.select`
  width:100%;
  height:45px;
`;

const Option = styled.option`

`;

const ListCategoriesBlock = styled.div`
  width:100%;
  margin-top:15px;
  opacity: ${ props => props.sub ? 1 : 0 };
  visibility: ${ props => props.sub ? 'visible' : 'hidden' };
  height: ${ props => props.sub ? 'auto' : 0 };

  transition:.37s all;
`;

const ItemCategory = styled.div`
  width:100%;
  padding:15px;
  box-sizing:border-box;
  background:#f5f5f5;
  border-bottom:1px solid rgba(0,0,0,0.08);
  position:relative;
  z-index:1;
  display:flex;
  flex-direction:column;
  justify-content:space-between;
  align-items: center;
  border:1px solid rgba(0,0,0,${props => props.border ? 0.06 : 0});
  border-radius:${props => props.border ? '8px' : '0px'};
  margin:${props => props.border ? '5px 0' : '0px'};

  transition:.22s all;

  &:hover {
    background:#f1f1f1;
    border:1px solid rgba(0,0,0,0.07);
    z-index:2;
    cursor:pointer;
  }
`;

const FormHead = styled.div`
  width:100%;
  display:flex;
  flex-direction:row;
  justify-content:space-between;
  align-items:center:

  > svg {
    cursor:pointer;
  }
`;

const ItemCategoryControll = styled.div`
  display:flex;
  flex-direction:row;
  justify-content:center;
  align-items:center;
`;

const UploadFileSection = styled.div`
  width:100%;
  margin-bottom:15px;
  display:table;
`;

const UploadFileButton = styled.div`
  position:relative;
  width:100%;
  height:45px;
  cursor:pointer;
  display:flex;
  justify-content:center;
  align-items:center;
  background:#eaeae9;
  border-radius:8px;
`;

const PreviewPicture = styled.div`
  width:150px;
  height:150px;
  border-radius:50%;
  margin-top:15px;
`;

const ItemCategoryLine = styled.div`
  width:100%;
  display:flex;
  flex-direction:row;
  align-items:center;
  justify-content:space-between;
`;

const ItemSubCategoryLine = styled.div`
  width:calc(100% - 20px);
  margin-left:20px;
  box-sizing:border-box;
  display:flex;
  flex-direction:row;
  align-items:center;
  justify-content:space-between;
  background:#fff;
  margin-bottom:5px;
  box-shadow:inset 0 0 15px rgba(0,0,0,0.07);
  border-radius:8px;
  padding:15px;

  transition:.2s all;

  &:hover {
    box-shadow:0 0 8px rgba(0,0,0,0.09);

    transition:.2s all;
  }
`;

const ItemSubCategories = styled.div`
  width:100%;
  border-top:1px solid rgba(0,0,0,0.1);
  margin-top:10px;
`;

const ItemCategoryLineLeft = styled.div`
  display:flex;
  flex-direction:row;
  align-items:center;
`;

const ItemCategoryLineLeftPicture = styled.div`
  width:35px;
  height:35px;
  border-radius:50%;
  margin-right:10px;
`;

export {
	Wrapper,
  H1,
  WrapperHead,
  WrapperHeadLeft,
  P,
  WrapperHeadRight,
  Button,
  WrapperBody,
  NoCategories,
  Form,
  SectionSubCategorySwitch,
  Input,
  Select,
  Option,
  ListCategoriesBlock,
  ItemCategory,
  FormHead,
  ItemCategoryControll,
  UploadFileSection,
  UploadInput,
  UploadFileButton,
  PreviewPicture,
  ItemSubCategories,
  ItemCategoryLine,
  ItemSubCategoryLine,
  ItemCategoryLineLeft,
  ItemCategoryLineLeftPicture
}