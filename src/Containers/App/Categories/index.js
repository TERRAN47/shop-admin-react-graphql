import React, {
	useState,
	useEffect,
	Suspense,
  useRef
} from 'react';

import { useTranslation } from "react-i18next";

import {
  useQuery,
  useSubscription,
  useMutation,
  useLazyQuery
} from '@apollo/client';

import { withRouter } from 'react-router-dom';
import { 
  BarMenu
} from '../../../Components';

import _ from 'lodash';

import {
	Wrapper,
  H1,
  WrapperHead,
  WrapperHeadLeft,
  P,
  WrapperHeadRight,
  Button,
  WrapperBody,
  NoCategories
} from './styled';

import { categoriesGQL } from '../../../Services/gqls';

import CreateCategoryForm from './Helpers/create-category-form';

import actions from './actions';
import Item from './Helpers/item';

const Categories = ({
	history
}) => {

  const childRef = useRef([]);

  const [categories, setCategories] = useState(null);
  const [modalCreateCategory, setModalCreateCategory] = useState(false);

  const { t } = useTranslation('common');

  const getCategories = useQuery(categoriesGQL.getCategories, {
    variables: {
      type:'MAIN'
    }
  });

  useEffect(()=>{

    if(!getCategories.loading){
      setCategories(getCategories.data.getCategories);
      
    }

  }, [
    getCategories.data, 
    getCategories.loading
  ])

  const removeCat = (id) => {

    let updateCategories = _.filter(categories, (item) => { return item._id !== id; });

    setCategories(updateCategories.length ? updateCategories : null);
    return 
  }

	return (
    <BarMenu>
      <Wrapper>

        <CreateCategoryForm closeModal={()=>setModalCreateCategory(!modalCreateCategory)} t={t} noCategoriesText={t('app.noCategories')} categories={categories} hide={modalCreateCategory} />

        <WrapperHead>
          <WrapperHeadLeft>
            <H1>{ t('app.categories') }</H1>
            <P>управление категориями</P>
          </WrapperHeadLeft>
          <WrapperHeadRight>
            <Button onClick={()=>setModalCreateCategory(!modalCreateCategory)} type="submit" value={t('app.add')} />
          </WrapperHeadRight>
        </WrapperHead>

        <WrapperBody noCategories={categories && categories.length ? false : true} >
          {
            categories && categories.length ?
              categories.map((cat, index)=>{
                return <Item ref={(ref)=>{
                  childRef.current[cat._id] = ref;
                  return
                }} useLazyQuery={useLazyQuery} key={cat._id} removeCat={(id)=>removeCat(id)} useMutation={useMutation} categoriesGQL={categoriesGQL} t={t} item={cat} />
              })
            :
              <NoCategories>
                <H1>{ t('app.noCategories') }</H1>
              </NoCategories>
          }
        </WrapperBody>

      </Wrapper>
    </BarMenu>
	)
}

export default withRouter(Categories);