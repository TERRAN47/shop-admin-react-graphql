import React, {
  useState,
  useEffect,
  forwardRef,
  useImperativeHandle
} from 'react';

import _ from 'lodash';

import {
  ItemCategory,
  P,
  H1,
  ItemCategoryControll,
  ItemSubCategories,
  ItemCategoryLine,
  ItemSubCategoryLine,
  ItemCategoryLineLeft,
  ItemCategoryLineLeftPicture
} from '../styled';

import config from '../../../../Config';

import Icon from '@mdi/react';
import {
  mdiDelete,
  mdiNotificationClearAll
} from '@mdi/js';

const Item = forwardRef(({
  item,
  t,
  categoriesGQL,
  useMutation,
  removeCat,
  useLazyQuery
}, ref) => {

  const [subCategories, setSubCategories] = useState(null);
  const [removeTypeCategory, setRemoveTypeCategory] = useState(null);
  const [openStatusSubCategories, setOpenStatusSubCategories] = useState(false);
  const [getSubCategories, { called, loading, data, refetch }] = useLazyQuery(categoriesGQL.getSubCategories);

  useEffect(()=>{

    if(data && data.getSubCategories.length){

      setSubCategories(data.getSubCategories);
    }

  }, [data, loading])

  const [removeCategory] = useMutation(categoriesGQL.removeCategory, {
    onCompleted: (data) => {

      if(removeTypeCategory != 'SUB'){
        removeCat(item._id);
        return
      }

      let updateSubCategories = _.filter(subCategories, (subItem) => { return subItem._id !== data.removeCategory.id; });
      setSubCategories(updateSubCategories.length ? updateSubCategories : null);
      return 
      
    }
  });

  const eventRemoveCategory = (typeCat, id) => {

    setRemoveTypeCategory(typeCat);

    let removeCat =  window.confirm(t('app.categoryProductsDdeleted'));
    if(removeCat){
      removeCategory({
        variables: {
          cat_id:id
        }
      });
    }
  }

  useImperativeHandle(ref, () => ({
    updateSubCategories() {

      if(refetch){
        refetch({
          id:item._id
        });
      }

      return

    }

  }));


  const eventLoadSubCategory = (id) => {

    setOpenStatusSubCategories(!openStatusSubCategories);

    getSubCategories({
      variables:{
        id
      }
    });

    return
  }

  return (
    <ItemCategory key={item._id} border={openStatusSubCategories}>
      <ItemCategoryLine>
        <ItemCategoryLineLeft>
          {
            item.picture && item.picture.url ?
              <ItemCategoryLineLeftPicture style={{
                background:`url(${config.public}${item.picture.url}) center / cover no-repeat`
              }} />
            :
              <></>
          }
          <P size={16}>{ item.title }</P>
        </ItemCategoryLineLeft>
        <ItemCategoryControll>
          <Icon onClick={()=>eventLoadSubCategory(item._id)} title="Подкатегории" size={0.9} color="#555" path={mdiNotificationClearAll} />
          <Icon onClick={()=>eventRemoveCategory('MAIN', item._id)} size={0.7} color="#d2421d" path={mdiDelete} />
        </ItemCategoryControll>
      </ItemCategoryLine>
      {
        openStatusSubCategories ?
          <>
            {
              loading ?
                <P>Загружаем</P>
              : subCategories && subCategories.length ?

                <ItemSubCategories>
                  <H1 marginLeft={20} marginBottom={7} marginTop={10} size={13}>Подкатегории</H1>
                  {
                    subCategories.map((itemSubCat, indexSubCat)=>{
                      return (
                        <ItemSubCategoryLine key={itemSubCat._id}>

                          <ItemCategoryLineLeft>
                            {
                              itemSubCat.picture && itemSubCat.picture.url ?
                                <ItemCategoryLineLeftPicture style={{
                                  background:`url(${config.public}${itemSubCat.picture.url}) center / cover no-repeat`
                                }} />
                              :
                                <></>
                            }
                            <P size={16}>{ itemSubCat.title }</P>
                          </ItemCategoryLineLeft>

                          <ItemCategoryControll>
                            <Icon onClick={()=>eventRemoveCategory('SUB', itemSubCat._id)} size={0.7} color="#d2421d" path={mdiDelete} />
                          </ItemCategoryControll>
                        </ItemSubCategoryLine>
                      )
                    })
                  }
                  </ItemSubCategories>
              :
                <P>Категорий не найдено</P> 
            }
          </>
        :
          <></>
      }

    </ItemCategory>
  )
})

export default Item;