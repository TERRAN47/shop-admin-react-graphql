import React, {
  useState
} from 'react';

import Switch from "react-switch";
import { NotificationManager } from 'react-notifications';

import {
  Form,
  H1,
  Input,
  P,
  SectionSubCategorySwitch,
  Select,
  Option,
  ListCategoriesBlock,
  FormHead,
  UploadFileSection,
  UploadInput,
  UploadFileButton,
  PreviewPicture
} from '../styled';

import ReactLoading from 'react-loading';

import config from '../../../../Config';

import { 
  categoriesGQL, 
  filesGQL
} from '../../../../Services/gqls';

import { 
  useMutation
} from '@apollo/client';

import Icon from '@mdi/react';
import { mdiClose } from '@mdi/js';

const CreateCategoryForm = ({
  hide,
  categories,
  noCategoriesText,
  t,
  closeModal
}) => {

  const [title, setTitle] = useState('');
  const [sub, setSub] = useState(false);
  const [selectCategory, setSelectCategory] = useState('none');
  const [picture, setPicture] = useState(null);

  const [createCategory] = useMutation(categoriesGQL.createCategory, {
    onCompleted: (data) => {
      setPicture(null)
      setTitle('');
      setSelectCategory('none');
      setSub(false);
      return
    },
    update: (store, data) => {



    }
  });

  const [uploadFile, uploadFileInformation] = useMutation(filesGQL.uploadFile, {
    onCompleted: (data) => {
      setPicture(data.uploadFile)
      return
    }
  });

  const eventCreateCategory = () => {
    if(sub && selectCategory == 'none'){
      NotificationManager.error(t('app.dontSelectCategory'), t('app.errorMessage'));
      return
    }

    if(!title){
      NotificationManager.error(t('app.formFields'), t('app.errorMessage'));
      return 
    }

    createCategory({
      variables: {
        category: {
          title,
          type: sub ? 'SUB' : "MAIN",
          parent_category: selectCategory,
          picture:picture ? picture._id : null
        }
      }
    })

    return

  }

  return (
    <Form hide={hide}>
      <FormHead>
        <H1>Добавить категорию</H1>
        <Icon size={1} onClick={()=>closeModal()} path={mdiClose} />
      </FormHead>
      <Input type="text" value={title} placeholder={ t('categories.categoryName') } onChange={(val)=>setTitle(val.target.value)} />

      <UploadFileSection>

        <UploadFileButton>

          <P>{ t('app.selectFile') }</P>

          <UploadInput accept="image/*" upload type="file" onChange={(val)=>uploadFile({
            variables: {
              file:val.target.files[0]
            }
          })} />
        </UploadFileButton>

        {
          uploadFileInformation.loading ?
            <PreviewPicture>
              <ReactLoading type="bubbles" color="rgb(45 75 225)" width="100%" />
            </PreviewPicture>
          : picture && picture.url ?
            <PreviewPicture style={{
              background:`url(${config.public}${picture.url}) center / cover no-repeat`
            }} />
          :
            <></>
        }

      </UploadFileSection>

      <SectionSubCategorySwitch>
        <P>Подкатегория</P>
        <Switch onChange={(checked)=>setSub(checked)} checked={sub} />
      </SectionSubCategorySwitch>

      <ListCategoriesBlock sub={sub}>
        {
          categories && categories.length ?

            <Select value={selectCategory} onChange={(val)=>{
              setSelectCategory(null);
              setSelectCategory(val.target.value)
              return
            }}>

              <Option value="none" >{ t('app.selectCategory') }</Option>

              {
                categories.map((cat, index)=>{
                  return <Option key={index} value={cat._id} >{ cat.title }</Option>
                })
              }
            </Select>
          :
            <P>{ noCategoriesText }</P>
        }
      </ListCategoriesBlock>

      <Input submit type="submit" onClick={() => eventCreateCategory()} value="Добавить" />

    </Form>
  )
}

export default CreateCategoryForm;