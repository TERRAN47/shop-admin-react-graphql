const getCategories = (query) => {
  let { data, loading } = query;
  return {
    getCategories:data,
    categoriesLoading:loading
  }
}

const addCategorySubscribe = (query) => {
  let { data, loading } = query;
  return {
    newCategory:data,
    newCategoryLoading:loading
  } 
}

export default {
  getCategories,
  addCategorySubscribe
}