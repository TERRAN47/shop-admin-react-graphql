import React from 'react';
import ReactDOM from 'react-dom';
import reportWebVitals from './reportWebVitals';
import { Route, HashRouter } from 'react-router-dom';

import { ApolloProvider } from '@apollo/react-hooks';
import { apolloClientSetup } from './Services';
import './Services/styled/default.css';

import Initial from './Containers/initial';
import {I18nextProvider} from "react-i18next";

import i18next from './Services/tools/i18n';

ReactDOM.render(
  	<React.StrictMode>
  		<I18nextProvider i18n={i18next}>
	  		<ApolloProvider client={apolloClientSetup()}>
	  			<HashRouter>
	    			<Route path="/" render={() => <Initial /> } />
	    		</HashRouter>
	    	</ApolloProvider>
    	</I18nextProvider>
  	</React.StrictMode>,
  	document.getElementById('root')
);

reportWebVitals();
