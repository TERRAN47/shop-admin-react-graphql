import React, {
    useState
  } from 'react';
  import { useTranslation } from "react-i18next";
  import {
      styled,
    constants
  } from '../../Services';
  import {
    ClientBlock,
    OrderBlock,
    CatImage,
    LeftBlock,
    HeaderProduct,
    ProdutDetales,
    ProductBlock,
    HoverIcon,
    InputInfo,
    EditImage,
    ProductItem,
    Block,
    CenterBlock,
    ProductContent,
    IconControll,
    ProductInfo, 
    Decription, 
    Image,
  } from './styled'
  import moment from "moment"
  import Icon from '@mdi/react'
  import config from '../../Config';
  import { mdiImageEditOutline , mdiCloseCircle, mdiPailRemove  } from '@mdi/js';
  import { productsGQL, filesGQL } from '../../Services/gqls';
  import { 
    useMutation
  } from '@apollo/client';

  let {
    containers,
    text
  } = styled;
  const ProductDetal = ({
    itemProduct,
    editStatus,
    detalProductChange,
  }) => {
    const { t } = useTranslation('common');
    const [product, setItemProduct] = useState({id: itemProduct._id, category: itemProduct.category._id, description: itemProduct.description, price: itemProduct.price, title: itemProduct.title, picture: itemProduct.picture._id});
    const [uplodRef, setUploadRef] = useState(null)
    const [uplodImage, setUplodImage] = useState('');

    const [uploadFile] = useMutation(filesGQL.uploadFile, {
      onCompleted: (data)=>{
        console.log(111, data);

        let item = {...product};
        item.picture = data.uploadFile._id;
        setItemProduct(item);
        setUplodImage(data.uploadFile.url ? data.uploadFile.url : '');
      },
      onError: (err)=> {
        console.log(222, err)
      }
    });
    const [saveProduct] = useMutation(productsGQL.saveProduct, {
      onCompleted: (data)=>{
        console.log(3333, data)
      },
      onError: (err)=>{
        console.log(222, err)
      }
    });

    const changeProduct = (val, type)=>{
      let item = {...product}

      switch(type) {
        case 'price':
          item.price = +(val)
          return setItemProduct(item)
      
        case 'title':
          item.title = val
          return setItemProduct(item)

        case 'description':
          item.description = val
          return setItemProduct(item)

        default: return null
      }
      
    }

    const save = ()=>{
      saveProduct({
        variables: {
          product
        }
      })
    }

    console.log(7777777777777, itemProduct.category, product)
    return (
      <ProductBlock>
        <HeaderProduct>
          <text.h2 color={constants.colors.main}>{t("app.product")}</text.h2>
          <Icon size={1.5} color={constants.colors.red} path={mdiCloseCircle} onClick={detalProductChange} />
        </HeaderProduct>
        <ProductContent>
          <LeftBlock>
            <ClientBlock>
              <CatImage url={config.public+itemProduct.category.picture.url} />
              <text.P>{t('app.category')}: {itemProduct.category.title}</text.P> 
              <text.P>{t('app.subCategory')}: {itemProduct.category.parent_category ? itemProduct.category.parent_category.title : t('app.no')}</text.P> 
            </ClientBlock>
            {
              editStatus ?
                <CenterBlock>
                  <containers.DefButton onClick={save}>{t("app.save")}</containers.DefButton>
                </CenterBlock>
              : null
            }

          </LeftBlock>
        <ProdutDetales>

          <ProductItem>
            {
              editStatus ?
                <EditImage onClick={()=>{uplodRef.click()}}>
                  <Image src={uplodImage ? config.public+uplodImage : itemProduct.picture && itemProduct.picture.url ? config.public+itemProduct.picture.url : ''} />
                  <HoverIcon>
                    <Icon size={1.5} color={constants.colors.white} path={mdiImageEditOutline} onClick={detalProductChange} />
                  </HoverIcon>
                  <input
                    type="file"
                    ref={(ref) => setUploadRef(ref)}
                    style={{display: 'none'}}
                    onChange={(val)=>uploadFile({
                      variables: {
                        file: val.target.files[0]
                      }
                    })} 
                  />
                </EditImage>
              :
                <Image src={itemProduct.picture && itemProduct.picture.url ? config.public+itemProduct.picture.url : ''} />
            }

            <ProductInfo>
              {
                editStatus ?
                  <InputInfo defaultValue={itemProduct.title} type="text" onChange={(val)=> changeProduct(val.target.value, 'title')} />
                : <text.P color={constants.colors.main}>{itemProduct.title}</text.P>
              }
              {
                editStatus ?
                  <Block>
                    <text.P>{t('app.price')}: </text.P>
                    <InputInfo defaultValue={itemProduct.price} onChange={(val)=> changeProduct(val.target.value, 'price')} type="text" />
                  </Block>
                : <text.P>{t('app.price')}: {itemProduct.price} {constants.currency}</text.P>
              }
              {
                editStatus ?
                  <Block>
                    <text.P>{t('app.description')}: </text.P>
                    <text.TextArea defaultValue={itemProduct.description} onChange={(val)=> changeProduct(val.target.value, 'description')} />
                  </Block>
                :  <Decription>{itemProduct.description}</Decription>
              }
            </ProductInfo>
          </ProductItem>

          </ProdutDetales>
        </ProductContent>
      </ProductBlock>
    )
  }
  
  export default ProductDetal;