import styled from 'styled-components'


const FindInput = styled.input`
  padding: 10px;
  border: none;
  font-size: 14px;
  width: 85%;
  border-radius: 5px;
`
const ClientBlock = styled.div`
  background: #fff;
  padding: 15px;
  border-radius: 15px;
`
const ProductBlock = styled.div`
  min-height: 80vh;
`
const ProductContent = styled.div`
  display: flex;
  align-items: flex-start;
`
const CatImage = styled.div`
  width: 105px;
  border-radius: 50%;
  height: 100px;
  background: url(${props => props.url}) center;
  background-size: contain;
  margin: 0 auto;

`
const CenterBlock = styled.div`
  display: table;
  margin: 15px auto;
`

const ProdutDetales = styled.div`
  width: 100%;
  margin-left: 15px;
  margin-top: 15px;
  background: #fff;
  padding: 15px;
  border-radius: 15px;
  
	@media (min-width: 1500px) {
		${props => props.statusMenu}
	}
`
const HeaderProduct = styled.div`
    display: flex;
    justify-content: space-between;
`
const Block = styled.div`
  width: ${props => props.width ? props.width : '100%'}; 
  position: relative;
`

const OrderInfo = styled.div`
    background: #fff;
    padding: 15px;
    border-radius: 15px;
    margin-top: 15px;
`
const LeftBlock = styled.div`
    width: 25%;
    margin-top: 15px;
`
const InputInfo = styled.input`
  padding: 10px;
  border: 1px solid #c1c1c1;
  border-radius: 5px;
  width: 100%;

`
const ProductItem = styled.div`
    display:flex;
    flex-direction:row;

    box-sizing:border-box;
    padding:10px;
    position:relative;
    cursor:pointer;
    border-radius: 10px;
    background:#fff;
    transition:.3s all;
    &:hover {
        transition:.3s all;
    }
	@media (min-width: 1500px) {
		${props => props.statusMenu}
	}
`
const Decription = styled.p`
  margin-top: 10px;
  font-size: 14px;
  overflow: hidden;
  height: 40px;
  padding-right: 15px;
`
const ProductInfo = styled.div`
  padding: 0 15px;
  position: relative;
  width: 100%;
  box-sizing:border-box;
  display: flex;
  flex-direction: column;
  align-items: flex-start;

  p {
    font-size:17px;
    color:#c6c6c6;
    margin:0px;
  }

  > p:nth-child(1) {
    font-size:20px;
    color:#373737;
    margin:0 0 5px 0;
  }
`
const Image = styled.div`
  border-radius: 9px;
  width: 300px;
  height:150px;
  background: #fff url(${props=> props.src}) center/cover no-repeat;
`
const IconControll = styled.div`
  position:absolute;
  top:${({paddingTop})=>paddingTop ? paddingTop + 'px' : '10px'};
  right:10px;
`
const HoverIcon = styled.div`
  position:absolute;
  top:0;
  right:0;
  bottom: 0;
  left: 0;
  display: flex;
  justify-content: center;
  align-items: center;
  transition:.3s all;

  &:hover {
    background: #0000007a;
    border-radius: 10px;
  }
`
const EditImage = styled.div`
  position: relative;

`

export {
    FindInput,
    IconControll,
    InputInfo,
    HeaderProduct,
    EditImage,
    HoverIcon,
    Decription,
    Image,
    CenterBlock,
    Block,
    OrderInfo,
    LeftBlock,
    ProductItem,
    ProductBlock,
    ProdutDetales,
    ProductInfo,
    CatImage,
    ClientBlock,
    ProductContent,
}