import React, {
	useEffect,
  useState
} from 'react';
import {Link} from 'react-router-dom'
import { mdiMenu, mdiCogs, mdiAccountGroup, mdiClose, mdiBell, mdiClipboardTextMultipleOutline, mdiPlaylistMinus } from '@mdi/js';
import Icon from '@mdi/react'
import {styled, constants} from '../../Services'
import {BarHead, BarContent} from './styled'
import { useTranslation } from "react-i18next";

let {
	containers,
	text,
  WrapperPage
} = styled;
let {P} = text
let intViewportWidth = window.innerWidth;
const BarMenu = ({children}) => {
  const { t, i18n } = useTranslation('common');
  const [barMenuStatus, setBarMenuStatus] = useState(false);
  let menuName = window.location.hash
  let paramActiveMenu = {
    background: constants.colors.hoverBgLight,
		borderLeft: '5px solid #7ca0de',
		color: constants.colors.main
  }
  const barMenuSwith = ()=>{
    setBarMenuStatus(!barMenuStatus)
  }
  //let category = arrayUrl[arrayUrl.length - 1];
  console.log(22, menuName)
	return (
    <containers.wrapper>
      {intViewportWidth < 1200 ? 
        <containers.barIcon onClick={barMenuSwith}>
           <Icon path={barMenuStatus ? mdiClose : mdiMenu} size={1.6} />
        </containers.barIcon>
        : null
      }
      <containers.appBarMenu statusMenu={barMenuStatus ? {left: 0} : {left: -intViewportWidth}}>
        <BarHead> 
          <containers.barAva></containers.barAva>
          <text.h2>Fast food app</text.h2>
         {intViewportWidth > 1200 ? <div style={{marginRight: 15}}>
            <Icon path={mdiMenu} size={1.6} />
          </div>: null}
        </BarHead>

        <BarContent className="bar-content">
          <Link to="/orders">
            <containers.barItem activeMenu={menuName == "#/orders" ? paramActiveMenu : {}}>
              <Icon path={mdiBell} size={1} />
              <P>{t("app.orderBoard")}</P>
            </containers.barItem>
          </Link>

          <Link to="/categories">
            <containers.barItem activeMenu={menuName == "#/categories" ? paramActiveMenu : {}}>
              <Icon path={mdiPlaylistMinus} size={1} />
              <P>{t("app.categories")}</P>
            </containers.barItem>
          </Link>

          <Link to="/products">
            <containers.barItem activeMenu={menuName == "#/products" ? paramActiveMenu : {}}>
              <Icon path={mdiClipboardTextMultipleOutline} size={1} />
              <P>{t("app.products")}</P>
            </containers.barItem>
          </Link>

          <Link to="/users">
            <containers.barItem activeMenu={menuName == "#/users" ? paramActiveMenu : {}}>
              <Icon path={mdiAccountGroup} size={1} />
              <P>{t("app.users")}</P>
            </containers.barItem>
          </Link>
          <Link to="/settings">
            <containers.barItem activeMenu={menuName == "#/settings" ? paramActiveMenu : {}}>
              <Icon path={mdiCogs} size={1} />
              <P>{t("app.settings")}</P>
            </containers.barItem>
          </Link>
        </BarContent>
      </containers.appBarMenu>

      <containers.WrapperPage>
        { children }
      </containers.WrapperPage>

    </containers.wrapper>
	)
}

export default BarMenu;