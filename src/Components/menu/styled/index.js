
import styled from 'styled-components'


const BarHead = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  margin-top: 15px;
`
const BarContent = styled.div`
  margin-top: 15px;
`

export {
  BarHead,
  BarContent
}

