import ru from './ru.json';
import en from './en.json';

const locales = {
    en: {
        common: en
    },
    ru: {
        common: ru
    },
}

export default locales;