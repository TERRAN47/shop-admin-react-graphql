import i18next from "i18next";

import locales from './locales';

let getLNG = localStorage.getItem('LANGUAGE');

i18next.init({
  interpolation: { escapeValue: false },
  lng: getLNG ? getLNG : "ru",
  resources: locales
});

export default i18next;