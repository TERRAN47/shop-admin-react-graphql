import config from '../../Config';

const initAuth = async (user) => {

	if(!user){
		await localStorage.removeItem(config.TOKEN);
	}

	await localStorage.setItem('isAuth', user ? true : false);

	return
}

export default initAuth;