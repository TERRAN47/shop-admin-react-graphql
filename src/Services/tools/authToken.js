import config from '../../Config';

const saveToken = async (token, isAuth) => {

	if(isAuth){
		await localStorage.setItem(config.TOKEN, token);
	}else {
		await localStorage.removeItem(config.TOKEN);
	}

	await localStorage.setItem('isAuth', isAuth);
	return
}

export default saveToken;