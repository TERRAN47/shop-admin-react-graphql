import config from '../../Config';

const statusAuth = async (token, isAuth) => {

	let getStatus =	await localStorage.getItem(config.TOKEN);

	return getStatus;
}

export default statusAuth;