
const colors = {
  grayDarck: '#828282',
  grayLight: '#f1f1f1',
  hoverBgLight: '#eaf1fd',
  second: "#888888",
  main: "#324bdd",
  button: "#2d4be1",
  white: "#ececec",
  shadow: "#dedede",
  red: '#f75252',
}

const currency = '₸'

const sizes = {
  default: "14px"
}

export default {
  colors,
  currency,
  sizes
}