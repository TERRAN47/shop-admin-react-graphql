import styled from 'styled-components'
import constants from '../constants';

const logoName = styled.h1`
    font-size: 25px;
    letter-spacing: 3px;
    color: #6334bf;
    border-bottom: 3px solid #4f0fcc;
    display:flex;
    flex-direction:row;
    align-items:center;
    margin-bottom:5px;

    & span {
    	display:block;
    	width:7px;
    	height:7px;
    	border-radius:50%;
    	background:#ca1862;
    	margin:0 3px;
    }
`

const logoSlogan = styled.p`
  font-size:13px;
  letter-spacing:5px;
  text-align:center;
  text-transform: uppercase;
  color: #333;
`

const a = styled.a`
    
`

const P = styled.p`
  color: ${props=> props.color ? props.color : '#000'};
  margin-top: 10px;
`

const h4 = styled.h4`
    
`
const tableHeadTd = styled.h4`
  font-weight: bold;
  width:  ${props => props.width ? props.width : '50px'};
`
const tableBodyTd = styled.p`
  font-size: 14px;
  width:  ${props => props.width ? props.width : '50px'};
`

const h2 = styled.h2`
  font-size:25px;
  font-weight:bold;

  color: ${props => props.color};
`

const TextArea = styled.textarea`
  font-size: 14px;
  width: 100%;
  border-radius: 8px;
  padding: 10px;
  border: 1px solid ${constants.colors.grayLight};
  box-sizing: border-box;
  
`
export default {
	logoName,
    TextArea,
    tableBodyTd,
    tableHeadTd,
	P,
	a,
	logoSlogan,
	h4,
    h2
}