import containers from './divs';
import text from './text';

const styled = {
	containers,
	text
}

export default styled;