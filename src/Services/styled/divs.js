import styled from 'styled-components'

import constants from '../constants';

let wrapper = styled.div`
  min-width: 1200px;
  max-width: 80%;
  min-height:calc(100vh);
  margin:0 auto;
  border:1px solid rgba(0,0,0,0.06);
  border-top-left-radius: 20px;
  border-top-right-radius: 20px;
  display: flex;
  background:#fff;
  box-shadow:0 0 24px rgba(0,0,0,0.1);
  overflow-y:auto;
  box-sizing:border-box;
  margin-top: 3%;
  z-index: 100;

  @media (max-width: 760px) {
	min-width: calc(100vw);
	min-height:calc(100vh);
	margin-top: 0;
  }
`
let appBarMenu = styled.div`
	width: 25%;
	z-index: 10000;
	box-shadow: 0 0 10px #e2e2e2;
	${props => props.statusMenu}
	@media (max-width: 760px) {
		width: 85vw;
		position: fixed;
		top: 0; left: -100vw;
		height: calc(100vh);
		z-index: 999999;
		background: #fff;
		transition: all .5s;
		${props => props.statusMenu}
	}

`
let FindInputBlock = styled.div`
    border: 1px solid #efefef;
    width: 40%;
    border-radius: 5px;
    display: flex;
	justify-content: space-between;
    align-items: center;
	@media (max-width: 760px) {
		width: 65%;
	}
`

const header = styled.header`
	width: 100%;
	box-sizing: border-box;
	min-height: 92px;
	display: flex;
	flex-direction: row;
	justify-content: space-between;
	box-shadow: 0 0 25px rgba(0,0,0,0.16);
	align-items: center;
`

const div = styled.div`

`
const row = styled.div`
	display: flex;

	@media (max-width: 760px) {
      flex-direction: column;
    }
`
const headerNavigation = styled.div`
	display:flex;
	flex-direction:row;
	align-items:center;

	& a {
	    margin-left: 10px;
	    color: #525252;
	    font-size: 17px;
	    font-style: normal;
	    padding:10px 15px;
	    border-radius:10px;

		background-position-y:0px;
		transition:all .5s;

		&:hover {
			box-shadow:0 0 20px rgba(0,0,0,0.12);
			background-position-y:-80px;
			transition:all .5s;
		}
	}
`
const headerLogo = styled.div`
	display:flex;
	flex-direction:	column;
`
const buttonsTop = styled.div`
	width:100%;
	display:flex;
	flex-direction:row;
	box-sizing:border-box;
	justify-content:space-between;

	& div {
		width:33%;
		height:110px;
		cursor:pointer;
		display:flex;
		justify-content:center;
		align-items:center;
		border:1px dashed rgb(79 15 204 / 13%);
		border-radius:20px;
		box-sizing:border-box;
		box-shadow:0 0 0 rgba(0,0,0,0);
		background-position-y:0px;
		transition:.4s all;

		&:hover {
			box-shadow:0 0 20px rgba(0,0,0,0.07);
			background-position-y:-70px;
			transition:.4s all;
		}
	}
`

const barIcon = styled.div`
  position: fixed;
  top: 10px;
  right: 10px;
  z-index: 9999;
	${
		props => props.activeBarIcon
	}

`
const barItem = styled.div`
	display: flex;
	align-items: center;
	font-size: 18px;
	cursor: pointer;
	padding: 15px;
	padding-left: 25px;
	border-left: 5px solid #fff;
	transition: all .7s;
	color: #828282;
	${
		props => props.activeMenu	
	}
	&:hover {
		background: ${constants.colors.hoverBgLight};
		border-left: 5px solid #7ca0de;
		color: ${constants.colors.main};
	}

	p{
		margin: 0 15px;
	}
`

const barAva = styled.div`
	height: 65px;
	width: 95px;
	background: url(/assets/logo.png) no-repeat center;  
	background-size: contain;
`
const contentBlock = styled.div`
	width: 100%;
`

const contentHeader = styled.div`
	padding: 15px;
	background: #fff;
	display: flex;
	justify-content: space-between;
`
const contentInfo = styled.div`
	padding: 15px;
	background: #f9f9f9;
	border-top-left-radius: 15px;
    border-top-right-radius: 15px;
`
const tableDefault = styled.div`
	background: #fff;
	margin-top: 20px;
	text-align: center;
	border-top-left-radius: 15px;
    border-top-right-radius: 15px;
`
const tableHead = styled.div`
	background: #fff;
	padding: 15px;
	display: flex;
	border-bottom: 1px solid #f1f1f1;
	border-top-left-radius: 15px;
    border-top-right-radius: 15px;
`
const tableBody = styled.div`
	
	
`
const tableBodyTr = styled.div`
	display: flex;
	padding: 15px;
	text-align: center;
	width: 100%;
	transition:all .3s;
	box-sizing: border-box;
	overflow: ${props => props.hidden ? props.hidden : 'hidden'};
	position: relative;
	border-bottom: 1px solid #f9f9f9;
	&:hover {
		background: ${constants.colors.hoverBgLight};
	}
`
const FindInput = styled.input`
    padding: 10px;
    border: none;
    font-size: 14px;
    width: 85%;
    border-radius: 5px;
`

const IconInput = styled.div`
	padding: 0 5px;
	cursor: pointer;
`
const IconItemRight = styled.div`
	padding: 5px;
	cursor: pointer;
	display: table;
	position: absolute;
	top: ${props => props.top ? props.top : '10px'};
	right: 0;
	box-shadow: 0 0 5px ${constants.colors.shadow};
    border-top-left-radius: 8px;
    border-bottom-left-radius: 8px;
	color: ${props => props.color ? props.color : constants.colors.grayDarck};
`

const WrapperPage = styled.div`
	width:100%;
	box-sizing:border-box;
	padding:15px;

	@media (max-width: 760px) {
		margin-top: 30px;
	}
`;
const DefSelect = styled.select`
	width: 300px;
	padding: 10px;
    border: none;
    margin-top: 15px;
`;
const Option = styled.option`
	padding: 10px;
    border: none;
`;
const DefButton = styled.button`
	padding: 10px 20px;
    border: none;
    background: ${constants.colors.main};
	color: #fff;
	cursor: pointer;
	border-radius: 8px;
	font-size: 14px;
`;


export default {
	FindInputBlock,
	DefButton,
	DefSelect,
	Option,
	FindInput,
	IconInput,
	IconItemRight,
	wrapper,
	tableBodyTr,
	tableBody,
	tableDefault,
	tableHead,
	contentHeader,
	contentInfo,
	contentBlock,
	barAva,
	barItem,
	barIcon,
	row,
	appBarMenu,
	header,
	div,
	headerLogo,
	headerNavigation,
	buttonsTop,
  	WrapperPage
}