import { gql } from '@apollo/client';

const getUsers = gql`
query($query: QueryInput) {
  getUsers(query: $query) {
    _id
    firstName
    email
    avatar
    phone
    createdAt 
  }
}
`;

export default {getUsers}