import { gql } from '@apollo/client';

const getSettings = gql`
  query {
    getSettings(query: {}) {
      type
      value
    }
  }
`;

const saveSettings = gql`
mutation ($setting: SettingInput!){
  saveSettings(
    setting: $setting
  ) {
      type
      value
    }
}`;
export default {getSettings, saveSettings}