import { gql } from '@apollo/client';

const getProducts = gql`
  query($query: QueryInput) {
    getProducts(query: $query) {
      _id
      title
      description
      price
      picture{
        url
        _id
      }
      category{
        title
        _id
        picture{
          url
        }
        parent_category{
          title
        }
      }
    }
  }
`;

const removeProduct= gql`
  mutation ($id: String!){
    removeProduct(
      id: $id
    ) {
      status
    }
  }
`;
const saveProduct= gql`
  mutation ($product: productUpdateInput!){
    updateProduct(
      product: $product
    ) {
      price
    }
  }
`;
const searchProduts = gql`
  query($query: QueryInput) {
    getProducts(query: $query) {
      _id
      title
      description
      price
      picture{
        url
        _id
      }
      category{
        title
        _id
        picture{
          url
        }
        parent_category{
          title
        }
      }
    }
  }
`;

const createProduct= gql`
  mutation ($product: productInput!){
    createProduct(
      product: $product
    ) {
      _id
      title
      description
      picture{
        url
      }
      price
      category{
        _id
      }
    }
  }
`;

export default {createProduct, searchProduts, saveProduct, removeProduct, getProducts}