import { gql } from '@apollo/client';

const uploadFile = gql`
  mutation ($file: Upload!){
    uploadFile(file: $file) {
      _id
      type
      url
    }
  }
`;

export default {
	uploadFile
}