import { gql } from '@apollo/client';

const login = gql`

	mutation ($profile: UserInput){
	  authentication(
	    profile: $profile
	  ) {
	    token
	  }
	}

`;

const getUser = gql`

	query {
	  getUser {
	    token
	    avatar
	    firstName
	    email
      createdAt
	  }
	}

`;

export default {
	login,
	getUser
}