import { gql } from '@apollo/client';

const getOrders = gql`
query($query: QueryInput) {
  getOrders(query: $query) {
    _id
    order_number
    user{
      firstName
      _id
    }
    price
    createdAt
    products{
      title
      _id
      price
      description
      category{
        _id
        title
        picture{
          url
        }
        parent_category{
          title
          _id 
        }
      }
      picture{
        url
      }
    }
  }
}
`;

export default {getOrders}