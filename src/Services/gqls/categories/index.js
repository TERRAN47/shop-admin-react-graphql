import { gql } from '@apollo/client';

const getCategories = gql`
  query ($type: String){
    getCategories (type: $type) {
      _id
      title
      type
      picture {
        url
      }
      parent_category {
        title
      }
    }

  }

`;

const getSubCategories = gql`
  query ($id: String){
    getSubCategories (id: $id) {
      _id
      title
      type
      picture {
        url
      }
      parent_category {
        title
        _id
      }
    }

  }

`;

const createCategory = gql`
  mutation ($category: categoryInput){
    createCategory(
      category: $category
    ) {
      _id
      title
      type
      parent_category {
        title
        _id
      }
      picture {
        url
      }
    }
  }
`;

const removeCategory = gql`
  mutation ($cat_id: String){
    removeCategory(
      cat_id: $cat_id
    ) {
      status
      id
    }
  }
`;

const addedCategory = gql`
  subscription {

    addedCategory {
      _id
      title
      type
      picture {
        url
      }
      parent_category {
        title
        _id
      }
    }

  }
`;

const removedCategory = gql`
  subscription {

    removedCategory {
      id
    }

  }
`;

const createProduct = gql`
  mutation ($product: productInput!){
    createProduct(
      product: $product
    ) {
      _id
      title
      type
      parent_category {
        title
      }
    }
  }
`;

export default {
	getCategories,
  createProduct,
  createCategory,
  addedCategory,
  removeCategory,
  removedCategory,
  getSubCategories
}