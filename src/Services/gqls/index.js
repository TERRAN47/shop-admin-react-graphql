import example from './example';
import authorizationGQL from './authorization';
import categoriesGQL from './categories';
import filesGQL from './files';
import productsGQL from './products'
import userGQL from './users'
import settingsGQL from './settings'
import ordersGQL from './orders'

export {
	example,
  ordersGQL,
  userGQL,
  productsGQL,
	authorizationGQL,
  categoriesGQL,
  filesGQL,
  settingsGQL,
}